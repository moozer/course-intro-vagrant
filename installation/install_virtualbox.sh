#!/usr/bin/env bash

REPO="deb [arch=amd64] http://download.virtualbox.org/virtualbox/debian buster contrib"
REPOFILE="/etc/apt/sources.list.d/virtualbox.list"

KEYFILE="oracle_vbox_2016.asc"
KEYURL="https://www.virtualbox.org/download/$KEYFILE"

if ! which gpg; then
	echo "Installing signing tool"
	apt-get install -y gnupg
else
	echo "Gnupg already installed"
fi

echo "Add $REPO to available repositories"
echo $REPO > $REPOFILE

echo "Fetch repo key from $KEYURL"
wget $KEYURL

echo "Add key to trusted keys"
apt-key add $KEYFILE

echo "remove key to avoid clutter"
rm $KEYFILE

echo "update repo cache"
apt-get update

echo "install virtualbox and kernel headers"
apt-get install -y virtualbox-6.1 



