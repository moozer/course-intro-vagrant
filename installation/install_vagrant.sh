#!/usr/bin/env bash

VAGRANT_VERSION="2.2.14"
VAGRANT_DEB="vagrant_${VAGRANT_VERSION}_x86_64.deb"
VAGRANT_DL_URL="https://releases.hashicorp.com/vagrant/${VAGRANT_VERSION}/${VAGRANT_DEB}"

echo Downloading vagrant version $VAGRANT_VERSION
wget $VAGRANT_DL_URL

echo Install vagrant
dpkg -i $VAGRANT_DEB

echo Fix tangling dependencies, if any
apt-get install -f -y
