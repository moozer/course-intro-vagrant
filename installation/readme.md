Install virtualbox
=================

The script `install_virtualbox.sh` does

1. update and upgrade
2. add the virtualbox repo
3. install virtualbox

must be run as root, ie. log in as root or use `sudo`

Install vagrant
==================

The script `install_vagrant.sh`

1. Downloads the vagrant .deb file
2. installs it
