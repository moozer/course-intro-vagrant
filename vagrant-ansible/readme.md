vagrant-ansible
===============

test project for using vangrant with ansible.

ansible has to installed before use. There are two options

1. `apt-get install ansible`
2. `apt-get install python3-pip` followed by `pip3 install ansible` (as user or root)

Either option works.

Resulting webserver is available at [http://localhost:8081](http://localhost:8081)

